﻿using MimeDetective;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minimage
{
    public abstract class Compressor
    {
        public string[] MimeTypes { get; }

        protected Compressor(string[] mimeTypes)
        {
            MimeTypes = mimeTypes;
        }


        public async Task<byte[]> Compress(byte[] stream)
        {
            var size = stream.Length;
            if (!MimeTypes.Contains(stream.GetFileType().Mime)) return stream;
            var com = await CompressImplementation(stream);
            return com.Length > size ? stream : com;
        }

        protected abstract Task<byte[]> CompressImplementation(byte[] stream);
    }
}